:experimental:
include::{partialsdir}/entities.adoc[]

[[ch-Managing_Users_and_Groups]]
= Managing Users and Groups

The control of users and groups is a core element of {MAJOROS} system administration. This chapter explains how to add, manage, and delete users and groups in the graphical user interface and on the command line, and covers advanced topics, such as creating group directories.

[[s1-users-groups-introduction]]
== Introduction to Users and Groups

While users can be either people (meaning accounts tied to physical users) or accounts which exist for specific applications to use, groups are logical expressions of organization, tying users together for a common purpose. Users within a group share the same permissions to read, write, or execute files owned by that group.

Each user is associated with a unique numerical identification number called a _user ID_ (*UID*). Likewise, each group is associated with a _group ID_ (*GID*). A user who creates a file is also the owner and group owner of that file. The file is assigned separate read, write, and execute permissions for the owner, the group, and everyone else. The file owner can be changed only by `root`, and access permissions can be changed by both the `root` user and file owner.

Additionally, {MAJOROS} supports _access control lists_ (*ACLs*) for files and directories which allow permissions for specific users outside of the owner to be set. For more information about this feature, see the [citetitle]_link:++https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/ch-Access_Control_Lists.html++[Access Control Lists]_ chapter of the [citetitle]_link:++https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/System_Administrators_Guide/index.html++[Red Hat Enterprise Linux 7 System Administrators Guide]_.

[[s2-users-groups-private-groups]]
=== User Private Groups
indexterm:[groups,user private]indexterm:[user private groups,groups]indexterm:[groups,tools for management of,groupadd]
{MAJOROS} uses a _user private group_ (_UPG_) scheme, which makes UNIX groups easier to manage. A user private group is created whenever a new user is added to the system. It has the same name as the user for which it was created and that user is the only member of the user private group.

User private groups make it safe to set default permissions for a newly created file or directory, allowing both the user and *the group of that user* to make modifications to the file or directory.

The setting which determines what permissions are applied to a newly created file or directory is called a _umask_ and is configured in the `/etc/bashrc` file. Traditionally on UNIX-based systems, the [command]#umask# is set to [command]#022#, which allows only the user who created the file or directory to make modifications. Under this scheme, all other users, *including members of the creator's group*, are not allowed to make any modifications. However, under the UPG scheme, this "`group protection`" is not necessary since every user has their own private group.

A list of all groups is stored in the `/etc/group` configuration file.

[[s2-users-groups-shadow-utilities]]
=== Shadow Passwords
indexterm:[passwords,shadow]indexterm:[shadow passwords,overview of]
In environments with multiple users, it is very important to use _shadow passwords_ provided by the [package]*shadow-utils* package to enhance the security of system authentication files. For this reason, the installation program enables shadow passwords by default.

The following is a list of the advantages shadow passwords have over the traditional way of storing passwords on UNIX-based systems:

* Shadow passwords improve system security by moving encrypted password hashes from the world-readable `/etc/passwd` file to `/etc/shadow`, which is readable only by the `root` user.

* Shadow passwords store information about password aging.

* Shadow passwords allow the `/etc/login.defs` file to enforce security policies.

Most utilities provided by the [package]*shadow-utils* package work properly whether or not shadow passwords are enabled. However, since password aging information is stored exclusively in the `/etc/shadow` file, some utilities and commands do not work without first enabling shadow passwords:

* The [command]#chage# utility for setting password-aging parameters. For details, see the link:++https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/Security_Guide/chap-Hardening_Your_System_with_Tools_and_Services.html#sec-Password_Security++[Password Security] section in the [citetitle]_Red{nbsp}Hat Enterprise{nbsp}Linux{nbsp}7 Security Guide_.

* The [command]#gpasswd# utility for administrating the `/etc/group` file.

* The [command]#usermod# command with the [option]`-e, --expiredate` or [option]`-f, --inactive` option.

* The [command]#useradd# command with the [option]`-e, --expiredate` or [option]`-f, --inactive` option.

[[s1-users-configui]]
== Managing Users in a Graphical Environment
indexterm:[users,user configuration]indexterm:[groups,group configuration]indexterm:[user configuration,viewing list of users]indexterm:[group configuration,viewing list of groups]indexterm:[the Users settings tool,user configuration]
The [application]*Users* utility allows you to view, modify, add, and delete local users in the graphical user interface.

[[s2-redhat-config-users-list]]
=== Using the Users Settings Tool

Press the kbd:[Super] key to enter the Activities Overview, type [command]#Users# and then press kbd:[Enter]. The [application]*Users* settings tool appears. The kbd:[Super] key appears in a variety of guises, depending on the keyboard and other hardware, but often as either the Windows or Command key, and typically to the left of the Spacebar.

To make changes to the user accounts, first select the btn:[Unlock] button and authenticate yourself as indicated by the dialog box that appears. Note that unless you have superuser privileges, the application will prompt you to authenticate as `root`. To add and remove users, select the btn:[+] and btn:[-] button respectively. To add a user to the administrative group `wheel`, change the Account Type from `Standard` to `Administrator`. To edit a user's language setting, select the language and a drop-down menu appears.

[[fig-managing-users]]
.The Users Settings Tool

image::managing_users.png[The Users settings tool]

The commandline for call this Gui is : 
[user@domain ~]$ kcmshell5 user_manager 
When a new user is created, the account is disabled until a password is set. The Add User menu contains the options to set a password by the administrator immediately, or to allow the user to choose a password at the first login.

[[s1-users-tools]]
== Using Command Line Tools
indexterm:[users,tools for management of,useradd]indexterm:[users,tools for management of,the Users setting tool]indexterm:[groups,tools for management of,groupadd]
Apart from the [application]*Users* settings tool described in xref:basic-system-configuration/Managing_Users_and_Groups.adoc#s1-users-configui[Managing Users in a Graphical Environment], which is designed for basic managing of users, you can use command line tools for managing users and groups that are listed in xref:Managing_Users_and_Groups.adoc#table-users-tools[Command line utilities for managing users and groups].

[[table-users-tools]]
.Command line utilities for managing users and groups

[options="header"]
|===
|Utilities|Description
|[command]#id#|Displays user and group IDs.
|[command]#useradd#, [command]#usermod#, [command]#userdel#|Standard utilities for adding, modifying, and deleting user accounts.
|[command]#groupadd#, [command]#groupmod#, [command]#groupdel#|Standard utilities for adding, modifying, and deleting groups.
|[command]#gpasswd#|Standard utility for administering the `/etc/group` configuration file.
|[command]#pwck#, [command]#grpck#|Utilities that can be used for verification of the password, group, and associated shadow files.
|[command]#pwconv#, [command]#pwunconv#|Utilities that can be used for the conversion of passwords to shadow passwords, or back from shadow passwords to standard passwords.
|[command]#grpconv#, [command]#grpunconv#|Similar to the previous, these utilities can be used for conversion of shadowed information for group accounts.
|===

[[s2-users-tools-users-add]]
=== Adding a New User
indexterm:[useradd command,user account creation using]indexterm:[adding,user]indexterm:[user configuration,command line configuration,useradd]
To add a new user to the system, type the following at a shell prompt as `root`:

[subs="quotes, macros"]
----
[command]#useradd# _options_ _username_
----

…where _options_ are command-line options as described in xref:Managing_Users_and_Groups.adoc#table-useradd-options[Common useradd command-line options].
indexterm:[user configuration,command line configuration,passwd]
By default, the [command]#useradd# command creates a locked user account. To unlock the account, run the following command as `root` to assign a password:

[subs="quotes, macros"]
----
[command]#passwd# _username_
----

Optionally, you can set a password aging policy. See xref:Managing_Users_and_Groups.adoc#s2-users-tools-password-aging[Enabling Password Aging] for information on how to enable password aging.

[[table-useradd-options]]
.Common useradd command-line options

[options="header"]
|===
|Option|Description
|[option]`-c`pass:attributes[{blank}] 'pass:attributes[{blank}]_comment_pass:attributes[{blank}]'|_comment_ can be replaced with any string. This option is generally used to specify the full name of a user.
|[option]`-d`pass:attributes[{blank}] pass:attributes[{blank}]_home_directory_|Home directory to be used instead of default `/home/pass:attributes[{blank}]_username_pass:attributes[{blank}]/`.
|[option]`-e`pass:attributes[{blank}] pass:attributes[{blank}]_date_|Date for the account to be disabled in the format YYYY-MM-DD.
|[option]`-f`pass:attributes[{blank}] pass:attributes[{blank}]_days_|Number of days after the password expires until the account is disabled. If `0` is specified, the account is disabled immediately after the password expires. If `-1` is specified, the account is not disabled after the password expires.
|[option]`-g`pass:attributes[{blank}] pass:attributes[{blank}]_group_name_|Group name or group number for the user's default (primary) group. The group must exist prior to being specified here.
|[option]`-G`pass:attributes[{blank}] pass:attributes[{blank}]_group_list_|List of additional (supplementary, other than default) group names or group numbers, separated by commas, of which the user is a member. The groups must exist prior to being specified here.
|[option]`-m`|Create the home directory if it does not exist.
|[option]`-M`|Do not create the home directory.
|[option]`-N`|Do not create a user private group for the user.
|[option]`-p`pass:attributes[{blank}] pass:attributes[{blank}]_password_|The password encrypted with [command]#crypt#.
|[option]`-r`|Create a system account with a UID less than 1000 and without a home directory.
|[option]`-s`|User's login shell, which defaults to [command]#/bin/bash#.
|[option]`-u`pass:attributes[{blank}] pass:attributes[{blank}]_uid_|User ID for the user, which must be unique and greater than 999.
|===

The command-line options associated with the [command]#usermod# command are essentially the same. Note that if you want to add a user to another supplementary group, you need to use the [option]`-a, --append` option with the [option]`-G` option. Otherwise the list of supplementary groups for the user will be overwritten by those specified with the [command]#usermod -G# command.

.Explaining the Process
The following steps illustrate what happens if the command [command]#useradd juan# is issued on a system that has shadow passwords enabled:

. A new line for `juan` is created in `/etc/passwd`:
+
[subs="quotes"]
----
juan:x:1001:1001::/home/juan:/bin/bash
----
+
The line has the following characteristics:
+
** It begins with the user name `juan`.
+
** There is an `x` for the password field indicating that the system is using shadow passwords.
+
** A UID greater than 999 is created. Under {MAJOROS}, UIDs below 1000 are reserved for system use and should not be assigned to users.
+
** A GID greater than 999 is created. Under {MAJOROS}, GIDs below 1000 are reserved for system use and should not be assigned to users.
+
** The optional _GECOS_ information is left blank. The GECOS field can be used to provide additional information about the user, such as their full name or phone number.
+
** The home directory for `juan` is set to `/home/juan/`.
+
** The default shell is set to [command]#/bin/bash#.

. A new line for `juan` is created in `/etc/shadow`:
+
[subs="quotes"]
----
juan:!!:14798:0:99999:7:::
----
+
The line has the following characteristics:
+
** It begins with the username `juan`.
+
** Two exclamation marks (`!!`) appear in the password field of the `/etc/shadow` file, which locks the account.
+
.Note
[NOTE]
====

If an encrypted password is passed using the [option]`-p` flag, it is placed in the `/etc/shadow` file on the new line for the user.

====
+
** The password is set to never expire.

. A new line for a group named `juan` is created in `/etc/group`:
+
[subs="quotes"]
----
juan:x:1001:
----
+
A group with the same name as a user is called a _user private group_. For more information on user private groups, see xref:Managing_Users_and_Groups.adoc#s2-users-groups-private-groups[User Private Groups].
+
The line created in `/etc/group` has the following characteristics:
+
** It begins with the group name `juan`.
+
** An `x` appears in the password field indicating that the system is using shadow group passwords.
+
** The GID matches the one listed for `juan`pass:attributes[{blank}]'s primary group in `/etc/passwd`.

. A new line for a group named `juan` is created in `/etc/gshadow`:
+
[subs="quotes"]
----
juan:!::
----
+
The line has the following characteristics:
+
** It begins with the group name `juan`.
+
** An exclamation mark (`!`) appears in the password field of the `/etc/gshadow` file, which locks the group.
+
** All other fields are blank.

. A directory for user `juan` is created in the `/home/` directory:
+
[subs="attributes"]
----
~]#{nbsp}ls -ld /home/juan
drwx------. 4 juan juan 4096 Mar  3 18:23 /home/juan
----
+
This directory is owned by user `juan` and group `juan`. It has _read_, _write_, and _execute_ privileges *only* for the user `juan`. All other permissions are denied.

. The files within the `/etc/skel/` directory (which contain default user settings) are copied into the new `/home/juan/` directory. The contents of `/etc/skel/` may vary depending on installed applications:
+
----
~]# ls -la /home/juan
total 24
drwx------. 4 juan juan 4096 Mar  3 18:23 .
drwxr-xr-x. 5 root root 4096 Mar  3 18:23 ..
-rw-r--r--. 1 juan juan   18 Jul 09 08:43 .bash_logout
-rw-r--r--. 1 juan juan  176 Jul 09 08:43 .bash_profile
-rw-r--r--. 1 juan juan  124 Jul 09 08:43 .bashrc
drwxr-xr-x. 4 juan juan 4096 Jul 09 08:43 .mozilla
----

At this point, a locked account called `juan` exists on the system. To activate it, the administrator must next assign a password to the account using the [command]#passwd# command and, optionally, set password aging guidelines.

[[s2-users-tools-groups-add]]
=== Adding a New Group
indexterm:[group configuration,groupadd]indexterm:[adding,group]
To add a new group to the system, type the following at a shell prompt as `root`:

[subs="macros"]
----
groupadd pass:quotes[_options_] pass:quotes[_group_name_]
----

…where _options_ are command-line options as described in xref:Managing_Users_and_Groups.adoc#table-groupadd-options[Common groupadd command-line options].

[[table-groupadd-options]]
.Common groupadd command-line options

[options="header"]
|===
|Option|Description
|[option]`-f`, [option]`--force`|When used with [option]`-g`pass:attributes[{blank}] pass:attributes[{blank}]_gid_ and _gid_ already exists, [command]#groupadd# will choose another unique _gid_ for the group.
|[option]`-g`pass:attributes[{blank}] pass:attributes[{blank}]_gid_|Group ID for the group, which must be unique and greater than 999.
|[option]`-K`, [option]`--key`pass:attributes[{blank}] pass:attributes[{blank}]_key_pass:attributes[{blank}]=pass:attributes[{blank}]_value_|Override `/etc/login.defs` defaults.
|[option]`-o`, [option]`--non-unique`|Allows creating groups with duplicate GID.
|[option]`-p`, [option]`--password`pass:attributes[{blank}] pass:attributes[{blank}]_password_|Use this encrypted password for the new group.
|[option]`-r`|Create a system group with a GID less than 1000.
|===

[[s2-users-tools-password-aging]]
=== Enabling Password Aging
indexterm:[password,expire]indexterm:[password,aging]indexterm:[expiration of password, forcing]indexterm:[chage command,forcing password expiration with]indexterm:[user configuration,password,forcing expiration of]
For security reasons, it is advisable to require users to change their passwords periodically. This can be done by using the [command]#chage# command.

.Shadow passwords must be enabled to use chage
[IMPORTANT]
====

Shadow passwords must be enabled to use the [command]#chage# command. For more information, see xref:Managing_Users_and_Groups.adoc#s2-users-groups-shadow-utilities[Shadow Passwords].

====
indexterm:[user configuration,command line configuration,chage]
To configure password expiration for a user from a shell prompt, run the following command as `root`:

[subs="quotes, macros"]
----
[command]#chage# _options_ _username_
----

…where _options_ are command line options as described in xref:Managing_Users_and_Groups.adoc#table-chage-options[chage command line options]. When the [command]#chage# command is followed directly by a username (that is, when no command line options are specified), it displays the specified users current password aging values and allows you to change these values interactively.

[[table-chage-options]]
.chage command line options

[options="header"]
|===
|Option|Description
|[option]`-d`pass:attributes[{blank}] pass:attributes[{blank}]_days_|Specifies the number of days since January 1, 1970 the password was changed.
|[option]`-E`pass:attributes[{blank}] pass:attributes[{blank}]_date_|Specifies the date on which the account is locked, in the format YYYY-MM-DD. Instead of the date, the number of days since January 1, 1970 can also be used.
|[option]`-I`pass:attributes[{blank}] pass:attributes[{blank}]_days_|Specifies the number of inactive days after the password expiration before locking the account. If the value is `0`, the account is not locked after the password expires.
|[option]`-l`|Lists current account aging settings.
|[option]`-m`pass:attributes[{blank}] pass:attributes[{blank}]_days_|Specify the minimum number of days after which the user must change passwords. If the value is `0`, the password does not expire.
|[option]`-M`pass:attributes[{blank}] pass:attributes[{blank}]_days_|Specify the maximum number of days for which the password is valid. When the number of days specified by this option plus the number of days specified with the [option]`-d` option is less than the current day, the user must change passwords before using the account.
|[option]`-W`pass:attributes[{blank}] pass:attributes[{blank}]_days_|Specifies the number of days before the password expiration date to warn the user.
|===

You can configure a password to expire the first time a user logs in. This forces users to change passwords immediately.

. Set up an initial password. There are two common approaches to this step: you can either assign a default password, or you can use a null password.
+
To assign a default password, type the following at a shell prompt as `root`:
+
[subs="quotes, macros"]
----
[command]#passwd# _username_
----
+
To assign a null password instead, use the following command:
+
[subs="quotes, macros"]
----
[command]#passwd# [option]`-d` _username_
----
+
.Avoid using null passwords whenever possible
[WARNING]
====

Using a null password, while convenient, is a highly insecure practice, as any third party can log in first and access the system using the insecure username. Always make sure that the user is ready to log in before unlocking an account with a null password.

====

. Force immediate password expiration by running the following command as `root`:
+
[subs="quotes, macros"]
----
[command]#chage# [option]`-d` [option]`0` _username_
----
+
This command sets the value for the date the password was last changed to the epoch (January 1, 1970). This value forces immediate password expiration no matter what password aging policy, if any, is in place.

Upon the initial log in, the user is now prompted for a new password.

[[s2-users-tools-users-logout]]
=== Enabling Automatic Logouts

Especially when the user is logged in as `root`, an unattended login session may pose a significant security risk. To reduce this risk, you can configure the system to automatically log out idle users after a fixed period of time:

. Make sure the [package]*screen* package is installed. You can do so by running the following command as `root`:
+
[subs="quotes, macros"]
----
[command]#dnf# [option]`install` [option]`screen`
----
+
For more information on how to install packages in {MAJOROS}, refer to xref:package-management/DNF.adoc#sec-Installing[Installing Packages].

. As `root`, add the following line at the beginning of the `/etc/profile` file to make sure the processing of this file cannot be interrupted:
+
[subs="quotes"]
----
trap "" 1 2 3 15
----

. Add the following lines at the end of the `/etc/profile` file to start a [command]#screen# session each time a user logs in to a virtual console or remotely:
+
[subs="quotes"]
----
SCREENEXEC="screen"
if [ -w $(tty) ]; then
  trap "exec $SCREENEXEC" 1 2 3 15
  echo -n 'Starting session in 10 seconds'
  sleep 10
  exec $SCREENEXEC
fi
----
+
Note that each time a new session starts, a message will be displayed and the user will have to wait ten seconds. To adjust the time to wait before starting a session, change the value after the [command]#sleep# command.

. Add the following lines to the `/etc/screenrc` configuration file to close the [command]#screen# session after a given period of inactivity:
+
[subs="quotes"]
----
idle 120 quit
autodetach off
----
+
This will set the time limit to 120 seconds. To adjust this limit, change the value after the [option]`idle` directive.
+
Alternatively, you can configure the system to only lock the session by using the following lines instead:
+
[subs="quotes"]
----
idle 120 lockscreen
autodetach off
----
+
This way, a password will be required to unlock the session.

The changes take effect the next time a user logs in to the system.

[[s2-users-tools-groups-directories]]
=== Creating Group Directories
indexterm:[groups,shared directories]indexterm:[user private groups,and shared directories]
System administrators usually like to create a group for each major project and assign people to the group when they need to access that project's files. With this traditional scheme, file management is difficult; when someone creates a file, it is associated with the primary group to which they belong. When a single person works on multiple projects, it becomes difficult to associate the right files with the right group. However, with the UPG scheme, groups are automatically assigned to files created within a directory with the _setgid_ bit set. The setgid bit makes managing group projects that share a common directory very simple because any files a user creates within the directory are owned by the group that owns the directory.

For example, a group of people need to work on files in the `/opt/myproject/` directory. Some people are trusted to modify the contents of this directory, but not everyone.

. As `root`, create the `/opt/myproject/` directory by typing the following at a shell prompt:
+
[subs="quotes, macros"]
----
[command]#mkdir /opt/myproject#
----

. Add the `myproject` group to the system:
+
[subs="quotes, macros"]
----
[command]#groupadd myproject#
----

. Associate the contents of the `/opt/myproject/` directory with the `myproject` group:
+
[subs="quotes, macros"]
----
[command]#chown root:myproject /opt/myproject#
----

. Allow users in the group to create files within the directory and set the setgid bit:
+
[subs="quotes, macros"]
----
[command]#chmod 2775 /opt/myproject#
----
+
At this point, all members of the `myproject` group can create and edit files in the `/opt/myproject/` directory without the administrator having to change file permissions every time users write new files. To verify that the permissions have been set correctly, run the following command:
+
[subs="attributes"]
----
~]#{nbsp}ls -ld /opt/myproject
drwxrwsr-x. 3 root myproject 4096 Mar  3 18:31 /opt/myproject

----

. Add users to the `myproject` group:
+
[subs="quotes, macros"]
----
[command]#usermod -aG myproject _username_pass:attributes[{blank}]#
----

[[sect-Users_and_Groups-Resources]]
== Additional Resources
indexterm:[groups,additional resources]indexterm:[users,additional resources]
For more information on how to manage users and groups on Fedora, see the resources listed below.

.Installed Documentationindexterm:[groups,additional resources,installed documentation]indexterm:[users,additional resources,installed documentation]
For information about various utilities for managing users and groups, see the following manual pages:

* `useradd`(8) — The manual page for the [command]#useradd# command documents how to use it to create new users.

* `userdel`(8) — The manual page for the [command]#userdel# command documents how to use it to delete users.

* `usermod`(8) — The manual page for the [command]#usermod# command documents how to use it to modify users.

* `groupadd`(8) — The manual page for the [command]#groupadd# command documents how to use it to create new groups.

* `groupdel`(8) — The manual page for the [command]#groupdel# command documents how to use it to delete groups.

* `groupmod`(8) — The manual page for the [command]#groupmod# command documents how to use it to modify group membership.

* `gpasswd`(1) — The manual page for the [command]#gpasswd# command documents how to manage the `/etc/group` file.

* `grpck`(8) — The manual page for the [command]#grpck# command documents how to use it to verify the integrity of the `/etc/group` file.

* `pwck`(8) — The manual page for the [command]#pwck# command documents how to use it to verify the integrity of the `/etc/passwd` and `/etc/shadow` files.

* `pwconv`(8) — The manual page for the [command]#pwconv#, [command]#pwunconv#, [command]#grpconv#, and [command]#grpunconv# commands documents how to convert shadowed information for passwords and groups.

* `id`(1) — The manual page for the [command]#id# command documents how to display user and group IDs.

For information about related configuration files, see:

* `group`(5) — The manual page for the `/etc/group` file documents how to use this file to define system groups.

* `passwd`(5) — The manual page for the `/etc/passwd` file documents how to use this file to define user information.

* `shadow`(5) — The manual page for the `/etc/shadow` file documents how to use this file to set passwords and account expiration information for the system.
