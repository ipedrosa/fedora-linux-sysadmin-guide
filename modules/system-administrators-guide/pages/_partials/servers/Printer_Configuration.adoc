
[[sec-Printer_Configuration]]
== Printer Configuration
indexterm:[CUPS,Printer Configuration]indexterm:[printers,Printer Configuration]
The [application]*Printers* configuration tool serves for printer configuring, maintenance of printer configuration files, print spool directories and print filters, and printer classes management.

The tool is based on the Common Unix Printing System (*CUPS*). If you upgraded the system from a previous {MAJOROS} version that used CUPS, the upgrade process preserved the configured printers.

.Using the CUPS web application or command-line tools
[NOTE]
====
indexterm:[Printer Configuration,CUPS]
You can perform the same and additional operations on printers directly from the CUPS web application or command line. To access the application, in a web browser, go to link:++http://localhost:631/++[http://localhost:631/]. For CUPS manuals refer to the links on the `Home` tab of the web site.

====

[[sec-Starting_Printer_Config]]
=== Starting the Printers Configuration Tool

With the [application]*Printers* configuration tool you can perform various operations on existing printers and set up new printers. You can also use CUPS directly (go to link:++http://localhost:631/++[http://localhost:631/] to access the CUPS web application).

To start the [application]*Printers* configuration tool if using the GNOME desktop, press the kbd:[Super] key to enter the Activities Overview, type [command]#Printers#, and then press kbd:[Enter]. The [application]*Printers* configuration tool appears. The kbd:[Super] key appears in a variety of guises, depending on the keyboard and other hardware, but often as either the Windows or Command key, and typically to the left of the kbd:[Spacebar].

The `Printers` window depicted in xref:File_and_Print_Servers.adoc#fig-printconf-main[Printers Configuration window] appears.

[[fig-printconf-main]]
.Printers Configuration window

image::printconf-main.png[Printers Configuration window]

[[sec-Setting_Printer]]
=== Starting Printer Setup
indexterm:[Printer Configuration,New Printer]
Printer setup process varies depending on the printer queue type.

If you are setting up a local printer connected with USB, the printer is discovered and added automatically. You will be prompted to confirm the packages to be installed and provide an administrator or the `root` user password. Local printers connected with other port types and network printers need to be set up manually.

[[proc-Setting_up_Printer]]

Follow this procedure to start a manual printer setup:

. Start the Printers configuration tool (refer to xref:File_and_Print_Servers.adoc#sec-Starting_Printer_Config[Starting the Printers Configuration Tool]).

. Select `Unlock` to enable changes to be made. In the `Authentication Required` box, type an administrator or the `root` user password and confirm.

. Select the plus sign to open the `Add a New Printer` dialog. Select the printer from the list or enter its address below.

[[sec-Adding_Other_Printer]]
=== Adding a Local Printer
indexterm:[Printer Configuration,Local Printers]
Follow this procedure to add a local printer connected with other than a serial port:

[[proc-Adding_Other_Printer]]

. Open the `Add a New Printer` dialog (refer to xref:File_and_Print_Servers.adoc#sec-Setting_Printer[Starting Printer Setup]).

. If the device does not appear automatically, select the port to which the printer is connected in the list on the left (such as `Serial Port #1` or `LPT #1`).

. On the right, enter the connection properties:

for `Enter URI`::  `URI` (for example file:/dev/lp0)

for `Serial Port`::  Baud Rate

Parity

Data Bits

Flow Control

.Adding a local printer

image::print_conf_window.png[Adding a local printer]

. Click btn:[Forward].

. Select the printer model. See xref:File_and_Print_Servers.adoc#s1-printing-select-model[Selecting the Printer Model and Finishing] for details.

[[s1-printing-jetdirect-printer]]
=== Adding an AppSocket/HP JetDirect printer

Follow this procedure to add an AppSocket/HP JetDirect printer:

[[proc-Adding_JetDirect_Printer]]

. Open the `Add a New Printer` dialog (refer to xref:File_and_Print_Servers.adoc#sec-Starting_Printer_Config[Starting the Printers Configuration Tool]).

. In the list on the left, select menu:Network Printer[pass:attributes[{blank}]`AppSocket/HP JetDirect`pass:attributes[{blank}]].

. On the right, enter the connection settings:

`Hostname`::  Printer host name or `IP` address.

`Port Number`::  Printer port listening for print jobs (`9100` by default).

[[fig-printconf-jetdirect]]
.Adding a JetDirect printer

image::printconf-jetdirect.png[Adding a JetDirect Printer]

. Click btn:[Forward].

. Select the printer model. See xref:File_and_Print_Servers.adoc#s1-printing-select-model[Selecting the Printer Model and Finishing] for details.

[[s1-printing-ipp-printer]]
=== Adding an IPP Printer
indexterm:[Printer Configuration,IPP Printers]
An `IPP` printer is a printer attached to a different system on the same TCP/IP network. The system this printer is attached to may either be running CUPS or simply configured to use `IPP`.

If a firewall is enabled on the printer server, then the firewall must be configured to allow incoming `TCP` connections on port `631`. Note that the CUPS browsing protocol allows client machines to discover shared CUPS queues automatically. To enable this, the firewall on the client machine must be configured to allow incoming `UDP` packets on port `631`.

[[proc-Adding_IPP_Printer]]

Follow this procedure to add an `IPP` printer:

. Open the `Printers` dialog (refer to xref:File_and_Print_Servers.adoc#sec-Setting_Printer[Starting Printer Setup]).

. In the list of devices on the left, select Network Printer and `Internet Printing Protocol (ipp)` or `Internet Printing Protocol (https)`.

. On the right, enter the connection settings:

`Host`::  The host name of the `IPP` printer.

`Queue`::  The queue name to be given to the new queue (if the box is left empty, a name based on the device node will be used).

[[fig-printconf-ipp]]
.Adding an IPP printer

image::printconf-ipp.png[Networked IPP Printer]

. Optionally, click btn:[Verify] to detect the printer.

. Click btn:[Forward] to continue.

. Select the printer model. See xref:File_and_Print_Servers.adoc#s1-printing-select-model[Selecting the Printer Model and Finishing] for details.

[[sec-printing-LPDLPR-printer]]
=== Adding an LPD/LPR Host or Printer
indexterm:[Printer Configuration,LDP/LPR Printers]

[[proc-Adding_HTTPS_Printer]]

Follow this procedure to add an LPD/LPR host or printer:

. Open the `New Printer` dialog (refer to xref:File_and_Print_Servers.adoc#sec-Setting_Printer[Starting Printer Setup]).

. In the list of devices on the left, select menu:Network Printer[pass:attributes[{blank}]`LPD/LPR Host or Printer`pass:attributes[{blank}]].

. On the right, enter the connection settings:

`Host`::  The host name of the LPD/LPR printer or host.

Optionally, click btn:[Probe] to find queues on the LPD host.

`Queue`::  The queue name to be given to the new queue (if the box is left empty, a name based on the device node will be used).

[[fig-printconf-lpd]]
.Adding an LPD/LPR printer

image::printconf-lpd.png[Adding an LPD/LPR Printer]

. Click btn:[Forward] to continue.

. Select the printer model. See xref:File_and_Print_Servers.adoc#s1-printing-select-model[Selecting the Printer Model and Finishing] for details.

[[s1-printing-smb-printer]]
=== Adding a Samba (SMB) printer
indexterm:[Printer Configuration,Samba Printers]indexterm:[Samba,Samba Printers]

[[proc-Adding_SMB_Printer]]

Follow this procedure to add a Samba printer:

.Installing the samba-client package
[NOTE]
====

Note that in order to add a Samba printer, you need to have the [package]*samba-client* package installed. You can do so by running, as `root`:

[subs="quotes, macros"]
----
[command]#dnf install samba-client#
----

For more information on installing packages with DNF, refer to xref:package-management/DNF.adoc#sec-Installing[Installing Packages].

====

. Open the `New Printer` dialog (refer to xref:File_and_Print_Servers.adoc#sec-Setting_Printer[Starting Printer Setup]).

. In the list on the left, select menu:Network Printer[pass:attributes[{blank}]`Windows Printer via SAMBA`pass:attributes[{blank}]].

. Enter the SMB address in the `smb://` field. Use the format _computer name/printer share_. In xref:File_and_Print_Servers.adoc#fig-printconf-smb[Adding a SMB printer], the _computer name_ is [command]#dellbox# and the _printer share_ is [command]#r2#.

[[fig-printconf-smb]]
.Adding a SMB printer

image::printconf-smb.png[SMB Printer]

. Click btn:[Browse] to see the available workgroups/domains. To display only queues of a particular host, type in the host name (NetBios name) and click btn:[Browse].

. Select either of the options:

.. `Prompt user if authentication is required`: user name and password are collected from the user when printing a document.

.. `Set authentication details now`: provide authentication information now so it is not required later. In the `Username` field, enter the user name to access the printer. This user must exist on the SMB system, and the user must have permission to access the printer. The default user name is typically `guest` for Windows servers, or `nobody` for Samba servers.

. Enter the `Password` (if required) for the user specified in the `Username` field.

.Be careful when choosing a password
[WARNING]
====

Samba printer user names and passwords are stored in the printer server as unencrypted files readable by `root` and the Linux Printing Daemon, `lpd`. Thus, other users that have `root` access to the printer server can view the user name and password you use to access the Samba printer.

Therefore, when you choose a user name and password to access a Samba printer, it is advisable that you choose a password that is different from what you use to access your local {MAJOROS} system.

If there are files shared on the Samba print server, it is recommended that they also use a password different from what is used by the print queue.

====

. Click btn:[Verify] to test the connection. Upon successful verification, a dialog box appears confirming printer share accessibility.

. Click btn:[Forward].

. Select the printer model. See xref:File_and_Print_Servers.adoc#s1-printing-select-model[Selecting the Printer Model and Finishing] for details.

[[s1-printing-select-model]]
=== Selecting the Printer Model and Finishing

Once you have properly selected a printer connection type, the system attempts to acquire a driver. If the process fails, you can locate or search for the driver resources manually.

[[proc-Selecting_Driver]]

Follow this procedure to provide the printer driver and finish the installation:

. In the window displayed after the automatic driver detection has failed, select one of the following options:

.. `Select printer from database` — the system chooses a driver based on the selected make of your printer from the list of `Makes`. If your printer model is not listed, choose `Generic`.

.. `Provide PPD file` — the system uses the provided _PostScript Printer Description_ (*PPD*) file for installation. A PPD file may also be delivered with your printer as being normally provided by the manufacturer. If the PPD file is available, you can choose this option and use the browser bar below the option description to select the PPD file.

.. `Search for a printer driver to download` — enter the make and model of your printer into the `Make and model` field to search on OpenPrinting.org for the appropriate packages.

[[fig-printconf-select-model]]
.Selecting a printer brand

image::printconf-select-model.png[Selecting a printer brand from the printer database brands.]

. Depending on your previous choice provide details in the area displayed below:

** Printer brand for the `Select printer from database` option.

** PPD file location for the `Provide PPD file` option.

** Printer make and model for the `Search for a printer driver to download` option.

. Click btn:[Forward] to continue.

. If applicable for your option, window shown in xref:File_and_Print_Servers.adoc#fig-printconf-select-driver[Selecting a printer model] appears. Choose the corresponding model in the `Models` column on the left.

.Selecting a printer driver
[NOTE]
====

On the right, the recommended printer driver is automatically selected; however, you can select another available driver. The print driver processes the data that you want to print into a format the printer can understand. Since a local printer is attached directly to your computer, you need a printer driver to process the data that is sent to the printer.

====

[[fig-printconf-select-driver]]
.Selecting a printer model

image::printconf-select-driver.png[Selecting a Printer Model with a Driver Menu]

. Click btn:[Forward].

. Under the `Describe Printer` enter a unique name for the printer in the `Printer Name` field. The printer name can contain letters, numbers, dashes (-), and underscores (_); it *must not* contain any spaces. You can also use the `Description` and `Location` fields to add further printer information. Both fields are optional, and may contain spaces.

[[fig-printconf-add]]
.Printer setup

image::printconf-add-printer.png[Printer Setup]

. Click btn:[Apply] to confirm your printer configuration and add the print queue if the settings are correct. Click btn:[Back] to modify the printer configuration.

. After the changes are applied, a dialog box appears allowing you to print a test page. Click btn:[Print Test Page] to print a test page now. Alternatively, you can print a test page later as described in xref:File_and_Print_Servers.adoc#s1-printing-test-page[Printing a Test Page].

[[s1-printing-test-page]]
=== Printing a Test Page

[[proc-Printing_Test_Page]]

After you have set up a printer or changed a printer configuration, print a test page to make sure the printer is functioning properly:

. Right-click the printer in the `Printing` window and click `Properties`.

. In the Properties window, click `Settings` on the left.

. On the displayed `Settings` tab, click the btn:[Print Test Page] button.

[[s1-printing-edit]]
=== Modifying Existing Printers

To delete an existing printer, in the `Printer` configuration window, select the printer and go to menu:Printer[pass:attributes[{blank}]`Delete`pass:attributes[{blank}]]. Confirm the printer deletion. Alternatively, press the kbd:[Delete] key.

To set the default printer, right-click the printer in the printer list and click the `Set As Default` button in the context menu.

==== The Settings Page
indexterm:[Printer Configuration,Settings]
To change printer driver configuration, double-click the corresponding name in the `Printer` list and click the `Settings` label on the left to display the `Settings` page.

You can modify printer settings such as make and model, print a test page, change the device location (URI), and more.

[[fig-printconf-config1]]
.Settings page

image::printconf-config1.png[Settings Page]

==== The Policies Page

Click the `Policies` button on the left to change settings in printer state and print output.

You can select the printer states, configure the `Error Policy` of the printer (you can decide to abort the print job, retry, or stop it if an error occurs).

You can also create a _banner page_ (a page that describes aspects of the print job such as the originating printer, the user name from the which the job originated, and the security status of the document being printed): click the `Starting Banner` or `Ending Banner` drop-down menu and choose the option that best describes the nature of the print jobs (for example, `confidential`).

[[sec-Sharing_Printers]]
.Sharing Printers
indexterm:[Printer Configuration,Sharing Printers]
On the `Policies` page, you can mark a printer as shared: if a printer is shared, users published on the network can use it. To allow the sharing function for printers, go to menu:Server[pass:attributes[{blank}]`Settings`pass:attributes[{blank}]] and select `Publish shared printers connected to this system`.

[[fig-printconf-config2]]
.Policies page

image::printconf-config2.png[Policies Page]

Make sure that the firewall allows incoming `TCP` connections to port `631`, the port for the Network Printing Server (`IPP`) protocol. To allow `IPP` traffic through the firewall on {MAJOROSVER}, make use of `firewalld`pass:attributes[{blank}]'s `IPP` service. To do so, proceed as follows:

[[proc-Enabling_IPP_Service_in_firewalld]]
.Enabling IPP Service in firewalld
. To start the graphical [application]*firewall-config* tool, press the kbd:[Super] key to enter the Activities Overview, type [command]#firewall# and then press kbd:[Enter]. The `Firewall Configuration` window opens. You will be prompted for an administrator or `root` password.

Alternatively, to start the graphical firewall configuration tool using the command line, enter the following command as `root` user:

[subs="attributes"]
----
~]#{nbsp}firewall-config
----

The `Firewall Configuration` window opens.

Look for the word "`Connected`" in the lower left corner. This indicates that the [application]*firewall-config* tool is connected to the user space daemon, `firewalld`.

To immediately change the current firewall settings, ensure the drop-down selection menu labeled `Configuration` is set to `Runtime`. Alternatively, to edit the settings to be applied at the next system start, or firewall reload, select `Permanent` from the drop-down list.

. Select the `Zones` tab and then select the firewall zone to correspond with the network interface to be used. The default is the `public` zone. The `Interfaces` tab shows what interfaces have been assigned to a zone.

. Select the `Services` tab and then select the `ipp` service to enable sharing. The `ipp-client` service is required for accessing network printers.

. Close the [application]*firewall-config* tool.

[[sec-The_Access_Control_Page]]
.The Access Control Page

You can change user-level access to the configured printer on the `Access Control` page. Click the `Access Control` label on the left to display the page. Select either `Allow printing for everyone except these users` or `Deny printing for everyone except these users` and define the user set below: enter the user name in the text box and click the btn:[Add] button to add the user to the user set.

[[fig-printconf-config3]]
.Access Control page

image::printconf-config3.png[Access Control Page]

[[sec-The_Printer_Options_Page]]
.The Printer Options Page

The `Printer Options` page contains various configuration options for the printer media and output, and its content may vary from printer to printer. It contains general printing, paper, quality, and printing size settings.

[[fig-printconf-config4]]
.Printer Options page

image::printconf-config4.png[Printer Options Page]

[[sec-Job_Options_Page]]
.Job Options Page

On the `Job Options` page, you can detail the printer job options. Click the `Job Options` label on the left to display the page. Edit the default settings to apply custom job options, such as number of copies, orientation, pages per side, scaling (increase or decrease the size of the printable area, which can be used to fit an oversize print area onto a smaller physical sheet of print medium), detailed text options, and custom job options.

[[fig-printconf-config5]]
.Job Options page

image::printconf-config5.png[Job Options Page]

[[sec-Ink_Toner_Levels_Page]]
.Ink/Toner Levels Page

The `Ink/Toner Levels` page contains details on toner status if available and printer status messages. Click the `Ink/Toner Levels` label on the left to display the page.

[[mediaobj-printconf-config6]]
.Ink/Toner Levels page

image::printconf-config6.png[Ink/Toner Levels Page]

[[s1-printing-managing]]
==== Managing Print Jobs
indexterm:[Printer Configuration,Print Jobs]
When you send a print job to the printer daemon, such as printing a text file from [application]*Emacs* or printing an image from [application]*GIMP*, the print job is added to the print spool queue. The print spool queue is a list of print jobs that have been sent to the printer and information about each print request, such as the status of the request, the job number, and more.

During the printing process, messages informing about the process appear in the notification area.

[[fig-gnome-print-manager-list]]
.GNOME Print Status

image::gnome-print-queue.png[GNOME Print Status]

To cancel, hold, release, reprint or authenticate a print job, select the job in the [application]*GNOME Print Status* and on the Job menu, click the respective command.

To view the list of print jobs in the print spool from a shell prompt, type the command [command]#lpstat -o#. The last few lines look similar to the following:

[[lpq-example]]
.Example of [command]#lpstat -o# output
====

[subs="quotes, macros"]
----
$ [command]#lpstat -o#
Charlie-60              twaugh            1024   Tue 08 Feb 2011 16:42:11 GMT
Aaron-61                twaugh            1024   Tue 08 Feb 2011 16:42:44 GMT
Ben-62                  root              1024   Tue 08 Feb 2011 16:45:42 GMT
----

====

If you want to cancel a print job, find the job number of the request with the command [command]#lpstat -o# and then use the command [command]#cancel _job number_pass:attributes[{blank}]#. For example, [command]#cancel 60# would cancel the print job in xref:File_and_Print_Servers.adoc#lpq-example[Example of [command]#lpstat -o# output]. You cannot cancel print jobs that were started by other users with the [command]#cancel# command. However, you can enforce deletion of such job by issuing the [command]#cancel -U root _job_number_pass:attributes[{blank}]# command. To prevent such canceling, change the printer operation policy to `Authenticated` to force `root` authentication.

You can also print a file directly from a shell prompt. For example, the command [command]#lp sample.txt# prints the text file `sample.txt`. The print filter determines what type of file it is and converts it into a format the printer can understand.

[[s1-printing-additional-resources]]
=== Additional Resources

To learn more about printing on {MAJOROS}, see the following resources.

[[s2-printing-additional-resources-installed]]
==== Installed Documentation

[command]#man lp#::  The manual page for the [command]#lpr# command that allows you to print files from the command line.

[command]#man cancel#::  The manual page for the command-line utility to remove print jobs from the print queue.

[command]#man mpage#::  The manual page for the command-line utility to print multiple pages on one sheet of paper.

[command]#man cupsd#::  The manual page for the CUPS printer daemon.

[command]#man cupsd.conf#::  The manual page for the CUPS printer daemon configuration file.

[command]#man classes.conf#::  The manual page for the class configuration file for CUPS.

[command]#man lpstat#::  The manual page for the [command]#lpstat# command, which displays status information about classes, jobs, and printers.

[[s2-printing-additional-resources-websites]]
==== Useful Websites

link:++https://wiki.linuxfoundation.org/openprinting/start++[]::  [citetitle]_Open Printing_ contains a large amount of information about printing in Linux.

link:++https://www.cups.org/++[]::  Documentation, FAQs, and newsgroups about CUPS.
