
:experimental:
include::{partialsdir}/entities.adoc[]

[[ch-Working_with_Kernel_Modules]]
= Working with Kernel Modules
indexterm:[kernel module,definition]indexterm:[module,kernel module]indexterm:[drivers,kernel module]
The Linux kernel is modular, which means it can extend its capabilities through the use of dynamically-loaded _kernel modules_. A kernel module can provide:

* a device driver which adds support for new hardware; or,

* support for a file system such as `btrfs` or `NFS`.

Like the kernel itself, modules can take parameters that customize their behavior, though the default parameters work well in most cases. User-space tools can list the modules currently loaded into a running kernel; query all available modules for available parameters and module-specific information; and load or unload (remove) modules dynamically into or from a running kernel. Many of these utilities, which are provided by the [package]*kmod* package, take module dependencies into account when performing operations so that manual dependency-tracking is rarely necessary.

On modern systems, kernel modules are automatically loaded by various mechanisms when the conditions call for it. However, there are occasions when it is necessary to load or unload modules manually, such as when one module is preferred over another although either could provide basic functionality, or when a module is misbehaving.

This chapter explains how to:

* use the user-space [application]*kmod* utilities to display, query, load and unload kernel modules and their dependencies;

* set module parameters both dynamically on the command line and permanently so that you can customize the behavior of your kernel modules; and,

* load modules at boot time.

.Installing the kmod package
[NOTE]
====

In order to use the kernel module utilities described in this chapter, first ensure the [package]*kmod* package is installed on your system by running, as root:

[subs="attributes"]
----
~]#{nbsp}dnf install kmod

----

For more information on installing packages with DNF, see xref:package-management/DNF.adoc#sec-Installing[Installing Packages].

====

[[sec-Listing_Currently-Loaded_Modules]]
== Listing Currently-Loaded Modules
indexterm:[kernel module,listing,currently loaded modules]indexterm:[kernel module,utilities,lsmod]indexterm:[lsmod,kernel module]
You can list all kernel modules that are currently loaded into the kernel by running the [command]#lsmod# command, for example:

----

~]$ lsmod
Module                  Size  Used by
tcp_lp                 12663  0
bnep                   19704  2
bluetooth             372662  7 bnep
rfkill                 26536  3 bluetooth
fuse                   87661  3
ip6t_rpfilter          12546  1
ip6t_REJECT            12939  2
ipt_REJECT             12541  2
xt_conntrack           12760  7
ebtable_nat            12807  0
ebtable_broute         12731  0
bridge                110196  1 ebtable_broute
stp                    12976  1 bridge
llc                    14552  2 stp,bridge
ebtable_filter         12827  0
ebtables               30913  3 ebtable_broute,ebtable_nat,ebtable_filter
ip6table_nat           13015  1
nf_conntrack_ipv6      18738  5
nf_defrag_ipv6         34651  1 nf_conntrack_ipv6
nf_nat_ipv6            13279  1 ip6table_nat
ip6table_mangle        12700  1
ip6table_security      12710  1
ip6table_raw           12683  1
ip6table_filter        12815  1
ip6_tables             27025  5 ip6table_filter,ip6table_mangle,ip6table_security,ip6table_nat,ip6table_raw
iptable_nat            13011  1
nf_conntrack_ipv4      14862  4
nf_defrag_ipv4         12729  1 nf_conntrack_ipv4
nf_nat_ipv4            13263  1 iptable_nat
nf_nat                 21798  4 nf_nat_ipv4,nf_nat_ipv6,ip6table_nat,iptable_nat
[output truncated]
----

Each row of [command]#lsmod# output specifies:

* the name of a kernel module currently loaded in memory;

* the amount of memory it uses; and,

* the sum total of processes that are using the module and other modules which depend on it, followed by a list of the names of those modules, if there are any. Using this list, you can first unload all the modules depending the module you want to unload. For more information, see xref:Working_with_Kernel_Modules.adoc#sec-Unloading_a_Module[Unloading a Module].

indexterm:[kernel module,files,/proc/modules]
Finally, note that [command]#lsmod# output is less verbose and considerably easier to read than the content of the `/proc/modules` pseudo-file.

[[sec-Displaying_Information_About_a_Module]]
== Displaying Information About a Module
indexterm:[kernel module,listing,module information]indexterm:[kernel module,utilities,modinfo]indexterm:[modinfo,kernel module]
You can display detailed information about a kernel module by running the [command]#modinfo{nbsp}pass:attributes[{blank}]_module_name_pass:attributes[{blank}]# command.

[[note-Module_names_do_not_end_in_.ko]]
.Module names do not end in .ko
[NOTE]
====

When entering the name of a kernel module as an argument to one of the [application]*kmod* utilities, do not append a `.ko` extension to the end of the name. Kernel module names do not have extensions; their corresponding files do.

====

[[ex-Listing_information_about_a_kernel_module_with_lsmod]]
.Listing information about a kernel module with lsmod
====

To display information about the `e1000e` module, which is the Intel PRO/1000 network driver, run:

----
~]# modinfo e1000e
filename:       /lib/modules/3.17.4-302.fc21.x86_64/kernel/drivers/net/ethernet/intel/e1000e/e1000e.ko
version:        2.3.2-k
license:        GPL
description:    Intel(R) PRO/1000 Network Driver
author:         Intel Corporation, <linux.nics@intel.com>
srcversion:     2FBED3F5E2EF40112284D95
alias:          pci:v00008086d00001503sv*sd*bc*sc*i*
alias:          pci:v00008086d00001502sv*sd*bc*sc*i*
[some alias lines omitted]
alias:          pci:v00008086d0000105Esv*sd*bc*sc*i*
depends:        ptp
intree:         Y
vermagic:       3.17.4-302.fc21.x86_64 SMP mod_unload
signer:         Fedora kernel signing key
sig_key:        1F:C9:E6:8F:74:19:55:63:48:FD:EE:2F:DE:B7:FF:9D:A6:33:7B:BF
sig_hashalgo:   sha256
parm:           debug:Debug level (0=none,...,16=all) (int)
parm:           copybreak:Maximum size of packet that is copied to a new buffer on receive (uint)
parm:           TxIntDelay:Transmit Interrupt Delay (array of int)
parm:           TxAbsIntDelay:Transmit Absolute Interrupt Delay (array of int)
parm:           RxIntDelay:Receive Interrupt Delay (array of int)
parm:           RxAbsIntDelay:Receive Absolute Interrupt Delay (array of int)
parm:           InterruptThrottleRate:Interrupt Throttling Rate (array of int)
parm:           IntMode:Interrupt Mode (array of int)
parm:           SmartPowerDownEnable:Enable PHY smart power down (array of int)
parm:           KumeranLockLoss:Enable Kumeran lock loss workaround (array of int)
parm:           WriteProtectNVM:Write-protect NVM [WARNING: disabling this can lead to corrupted NVM] (array of int)
parm:           CrcStripping:Enable CRC Stripping, disable if your BMC needs the CRC (array of int)
----

====

Here are descriptions of a few of the fields in [command]#modinfo# output:

filename::  The absolute path to the `.ko` kernel object file. You can use [command]#modinfo -n# as a shortcut command for printing only the `filename` field.

description::  A short description of the module. You can use [command]#modinfo -d# as a shortcut command for printing only the description field.

alias::  The `alias` field appears as many times as there are aliases for a module, or is omitted entirely if there are none.

depends::  This field contains a comma-separated list of all the modules this module depends on.
+
[[note-Omitted_depends_field]]
.Omitting the depends field
[NOTE]
====

If a module has no dependencies, the `depends` field may be omitted from the output.

====

parm::  Each `parm` field presents one module parameter in the form `pass:attributes[{blank}]_parameter_name_:pass:attributes[{blank}]_description_pass:attributes[{blank}]﻿`, where:
+
** _parameter_name_ is the exact syntax you should use when using it as a module parameter on the command line, or in an option line in a `.conf` file in the `/etc/modprobe.d/` directory; and,
+
** _description_ is a brief explanation of what the parameter does, along with an expectation for the type of value the parameter accepts (such as `int`, `unit` or `array of int`) in parentheses.
+
[[ex-Listing_module_parameters]]
.Listing module parameters
====

You can list all parameters that the module supports by using the [option]`-p` option. However, because useful value type information is omitted from [command]#modinfo -p# output, it is more useful to run:

----
~]# modinfo e1000e | grep "^parm" | sort
parm:           copybreak:Maximum size of packet that is copied to a new buffer on receive (uint)
parm:           CrcStripping:Enable CRC Stripping, disable if your BMC needs the CRC (array of int)
parm:           debug:Debug level (0=none,...,16=all) (int)
parm:           InterruptThrottleRate:Interrupt Throttling Rate (array of int)
parm:           IntMode:Interrupt Mode (array of int)
parm:           KumeranLockLoss:Enable Kumeran lock loss workaround (array of int)
parm:           RxAbsIntDelay:Receive Absolute Interrupt Delay (array of int)
parm:           RxIntDelay:Receive Interrupt Delay (array of int)
parm:           SmartPowerDownEnable:Enable PHY smart power down (array of int)
parm:           TxAbsIntDelay:Transmit Absolute Interrupt Delay (array of int)
parm:           TxIntDelay:Transmit Interrupt Delay (array of int)
parm:           WriteProtectNVM:Write-protect NVM [WARNING: disabling this can lead to corrupted NVM] (array of int)
----

====

[[sec-Loading_a_Module]]
== Loading a Module
indexterm:[kernel module,loading,for the current session]indexterm:[kernel module,utilities,modprobe]indexterm:[modprobe,kernel module]
To load a kernel module, run [command]#modprobe _module_name_pass:attributes[{blank}]# as `root`. For example, to load the `wacom` module, run:

----
~]# modprobe wacom

----
indexterm:[kernel module,directories,/lib/modules/kernel_version/kernel/drivers/]
By default, [command]#modprobe# attempts to load the module from `/lib/modules/pass:attributes[{blank}]_kernel_version_pass:attributes[{blank}]/kernel/drivers/`. In this directory, each type of module has its own subdirectory, such as `net/` and `scsi/`, for network and SCSI interface drivers respectively.

Some modules have dependencies, which are other kernel modules that must be loaded before the module in question can be loaded. The [command]#modprobe# command always takes dependencies into account when performing operations. When you ask [command]#modprobe# to load a specific kernel module, it first examines the dependencies of that module, if there are any, and loads them if they are not already loaded into the kernel. [command]#modprobe# resolves dependencies recursively: it will load all dependencies of dependencies, and so on, if necessary, thus ensuring that all dependencies are always met.

You can use the [option]`-v` (or [option]`--verbose`) option to cause [command]#modprobe# to display detailed information about what it is doing, which can include loading module dependencies.

[[ex-modprobe_-v_shows_module_dependencies_as_they_are_loaded]]
.modprobe -v shows module dependencies as they are loaded
====

You can load the `Fibre Channel over Ethernet` module verbosely by typing the following at a shell prompt:

----
~]# modprobe -v fcoe
insmod /lib/modules/3.17.4-302.fc21.x86_64/kernel/drivers/scsi/scsi_transport_fc.ko.xz
insmod /lib/modules/3.17.4-302.fc21.x86_64/kernel/drivers/scsi/libfc/libfc.ko.xz
insmod /lib/modules/3.17.4-302.fc21.x86_64/kernel/drivers/scsi/fcoe/libfcoe.ko.xz
insmod /lib/modules/3.17.4-302.fc21.x86_64/kernel/drivers/scsi/fcoe/fcoe.ko.xz
----

In this example, you can see that [command]#modprobe# loaded the `scsi_tgt`, `scsi_transport_fc`, `libfc` and `libfcoe` modules as dependencies before finally loading `fcoe`. Also note that [command]#modprobe# used the more primitive [command]#insmod# command to insert the modules into the running kernel.

====
indexterm:[kernel module,utilities,insmod]indexterm:[insmod,kernel module]

[[important-Always_use_modprobe_instead_of_insmod]]
.Always use modprobe instead of insmod!
[IMPORTANT]
====

Although the [command]#insmod# command can also be used to load kernel modules, it does not resolve dependencies. Because of this, you should *always* load modules using [command]#modprobe# instead.

====

[[sec-Unloading_a_Module]]
== Unloading a Module
indexterm:[kernel module,unloading]indexterm:[kernel module,utilities,modprobe]indexterm:[modprobe,kernel module]
You can unload a kernel module by running [command]#modprobe -r _module_name_pass:attributes[{blank}]#  as `root`. For example, assuming that the `wacom` module is already loaded into the kernel, you can unload it by running:

----
~]# modprobe -r wacom

----

However, this command will fail if a process is using:

* the `wacom` module;

* a module that `wacom` directly depends on, or;

* any module that `wacom`, through the dependency tree, depends on indirectly.

See xref:Working_with_Kernel_Modules.adoc#sec-Listing_Currently-Loaded_Modules[Listing Currently-Loaded Modules] for more information about using [command]#lsmod# to obtain the names of the modules which are preventing you from unloading a certain module.

[[ex-unloading_a_kernel_module]]
.Unloading a kernel module
====

For example, if you want to unload the `firewire_ohci` module, your terminal session might look similar to this:

----
~]# modinfo -F depends firewire_ohci
firewire-core
~]# modinfo -F depends firewire_core
crc-itu-t
~]# modinfo -F depends crc-itu-t

----

You have figured out the dependency tree (which does not branch in this example) for the loaded Firewire modules: `firewire_ohci` depends on `firewire_core`, which itself depends on `crc-itu-t`.

You can unload `firewire_ohci` using the [command]#modprobe -v -r _module_name_pass:attributes[{blank}]# command, where [option]`-r` is short for [option]`--remove` and [option]`-v` for [option]`--verbose`:

----
~]# modprobe -r -v firewire_ohci
rmmod firewire_ohci
rmmod firewire_core
rmmod crc_itu_t
----

The output shows that modules are unloaded in the reverse order that they are loaded, given that no processes depend on any of the modules being unloaded.

====
indexterm:[kernel module,utilities,rmmod]indexterm:[rmmod,kernel module]

[[important-Do_not_use_rmmod_directly]]
.Do not use rmmod directly!
[IMPORTANT]
====

Although the [command]#rmmod# command can be used to unload kernel modules, it is recommended to use [command]#modprobe -r# instead.

====

[[sec-Setting_Module_Parameters]]
== Setting Module Parameters
indexterm:[module parameters,kernel module]indexterm:[kernel module,module parameters,supplying]
Like the kernel itself, modules can also take parameters that change their behavior. Most of the time, the default ones work well, but occasionally it is necessary or desirable to set custom parameters for a module. Because parameters cannot be dynamically set for a module that is already loaded into a running kernel, there are two different methods for setting them.

. You can unload all dependencies of the module you want to set parameters for, unload the module using [command]#modprobe -r#, and then load it with [command]#modprobe# along with a list of customized parameters. This method is often used when the module does not have many dependencies, or to test different combinations of parameters without making them persistent, and is the method covered in this section.

. Alternatively, you can list the new parameters in an existing or newly created file in the `/etc/modprobe.d/` directory. This method makes the module parameters persistent by ensuring that they are set each time the module is loaded, such as after every reboot or [command]#modprobe# command. This method is covered in xref:Working_with_Kernel_Modules.adoc#sec-Persistent_Module_Loading[Persistent Module Loading], though the following information is a prerequisite.

[[ex-Supplying_optional_parameters_when_loading_a_kernel_module]]
.Supplying optional parameters when loading a kernel module
====

You can use [command]#modprobe# to load a kernel module with custom parameters using the following command line format:

[subs="attributes"]
----
~]#{nbsp}modprobe{nbsp}module_name{nbsp}parameter=value

----

====

When loading a module with custom parameters on the command line, be aware of the following:

* You can enter multiple parameters and values by separating them with spaces.

* Some module parameters expect a list of comma-separated values as their argument. When entering the list of values, do *not* insert a space after each comma, or [command]#modprobe# will incorrectly interpret the values following spaces as additional parameters.

* The [command]#modprobe# command silently succeeds with an exit status of 0 if:
+
** it successfully loads the module, *or*
+
** the module is *already* loaded into the kernel.
+
Thus, you must ensure that the module is not already loaded before attempting to load it with custom parameters. The [command]#modprobe# command does not automatically reload the module, or alert you that it is already loaded.

Here are the recommended steps for setting custom parameters and then loading a kernel module. This procedure illustrates the steps using the `e1000e` module, which is the network driver for Intel PRO/1000 network adapters, as an example:

[[proc-Loading_a_Kernel_Module_with_Custom_Parameters]]
.Loading a Kernel Module with Custom Parameters
. First, ensure the module is not already loaded into the kernel:
+
[subs="attributes"]
----
~]#{nbsp}lsmod |grep e1000e
~]#{nbsp}
----
+
Output would indicate that the module is already loaded into the kernel, in which case you must first unload it before proceeding. See xref:Working_with_Kernel_Modules.adoc#sec-Unloading_a_Module[Unloading a Module] for instructions on safely unloading it.

. Load the module and list all custom parameters after the module name. For example, if you wanted to load the Intel PRO/1000 network driver with the interrupt throttle rate set to 3000 interrupts per second for the first, second, and third instances of the driver, and turn on debug, you would run, as `root`:
+
[subs="attributes"]
----
~]#{nbsp}modprobe e1000e InterruptThrottleRate=3000,3000,3000 debug=1

----
+
This example illustrates passing multiple values to a single parameter by separating them with commas and omitting any spaces between them.

[[sec-Persistent_Module_Loading]]
== Persistent Module Loading
indexterm:[kernel module,loading,at the boot time]indexterm:[kernel module,directories,/etc/modules-load.d/]
As shown in xref:Working_with_Kernel_Modules.adoc#ex-Listing_information_about_a_kernel_module_with_lsmod[Listing information about a kernel module with lsmod], many kernel modules are loaded automatically at boot time. You can specify additional modules to be loaded by the `systemd-modules-load.service` daemon by creating a `pass:attributes[{blank}]_program_.conf` file in the `/etc/modules-load.d/` directory, where _program_ is any descriptive name of your choice. The files in `/etc/modules-load.d/` are text files that list the modules to be loaded, one per line.

[[ex-A_Text_File_to_Load_a_Module]]
.A Text File to Load a Module
====

To create a file to load the `virtio-net.ko` module, create a file `/etc/modules-load.d/virtio-net.conf` with the following content:

----
# Load virtio-net.ko at boot
virtio-net
----

====

See the `modules-load.d(5)` and `systemd-modules-load.service(8)` man pages for more information.

[[sect-signing-kernel-modules-for-secure-boot]]
== Signing Kernel Modules for Secure Boot

Fedora includes support for the UEFI Secure Boot feature, which means that Fedora can be installed and run on systems where UEFI Secure Boot is enabled. footnote:[Fedora does not require the use of Secure Boot on UEFI systems.] When Secure Boot is enabled, the EFI operating system boot loaders, the Fedora kernel, and all kernel modules must be signed with a private key and authenticated with the corresponding public key. The Fedora distribution includes signed boot loaders, signed kernels, and signed kernel modules. In addition, the signed first-stage boot loader and the signed kernel include embedded Fedora public keys. These signed executable binaries and embedded keys enable Fedora to install, boot, and run with the Microsoft UEFI Secure Boot CA keys that are provided by the UEFI firmware on systems that support UEFI Secure Boot.footnote:[Not all UEFI-based systems include support for Secure Boot.]

The information provided in the following sections describes steps necessary to enable you to self-sign privately built kernel modules for use with Fedora on UEFI-based systems where Secure Boot is enabled. These sections also provide an overview of available options for getting your public key onto the target system where you want to deploy your kernel module.

[[sect-prerequisites]]
=== Prerequisites

In order to enable signing of externally built modules, the tools listed in the following table are required to be installed on the system.

[[table-required-tools]]
.Required Tools

[options="header"]
|===
|Tool|Provided by Package|Used on|Purpose
|[command]#openssl#|[package]*openssl*|Build system|Generates public and private X.509 key pair
|[command]#sign-file#|[package]*kernel-devel*|Build system|C application used to sign kernel modules
|[command]#mokutil#|[package]*mokutil*|Target system|Optional tool used to manually enroll the public key
|[command]#keyctl#|[package]*keyutils*|Target system|Optional tool used to display public keys in the system key ring
|===

[NOTE]
====

Note that the build system, where you build and sign your kernel module, does not need to have UEFI Secure Boot enabled and does not even need to be a UEFI-based system.

====

[[sect-kernel-module-authentication]]
=== Kernel Module Authentication

In Fedora, when a kernel module is loaded, the module's signature is checked using the public X.509 keys on the kernel's system key ring, excluding those keys that are on the kernel's system black list key ring.

[[sect-sources-for-public-keys-used-to-authenticate-kernel-modules]]
==== Sources For Public Keys Used To Authenticate Kernel Modules

During boot, the kernel loads X.509 keys into the system key ring or the system black list key ring from a set of persistent key stores as shown in xref:Working_with_Kernel_Modules.adoc#table-sources-for-system-key-rings[Sources For System Key Rings]

[[table-sources-for-system-key-rings]]
.Sources For System Key Rings

[options="header"]
|===
|Source of X.509 Keys|User Ability to Add Keys|UEFI Secure Boot State|Keys Loaded During Boot
|Embedded in kernel|No|-|`.system_keyring`
.2+|UEFI Secure Boot "`db`"
.2+|Limited
|Not enabled|No
|Enabled|`.system_keyring`
.2+|UEFI Secure Boot "`dbx`"
.2+|Limited
|Not enabled|No
|Enabled|`.system_keyring`
.2+|Embedded in `shim.efi` boot loader
.2+|No
|Not enabled|No
|Enabled|`.system_keyring`
.2+|Machine Owner Key (MOK) list
.2+|Yes
|Not enabled|No
|Enabled|`.system_keyring`
|===

Note that if the system is not UEFI-based or if UEFI Secure Boot is not enabled, then only the keys that are embedded in the kernel are loaded onto the system key ring and you have no ability to augment that set of keys without rebuilding the kernel. The system black list key ring is a list of X.509 keys which have been revoked. If your module is signed by a key on the black list then it will fail authentication even if your public key is in the system key ring.

To confirm if Secure Boot is enabled, enter a command as follows:

[subs="quotes, macros"]
----
~]$ [command]#mokutil --sb-state#
SecureBoot enabled
----

If Secure Boot is not enabled then the message `Failed to read SecureBoot` is displayed.

You can display information about the keys on the system key rings using the [command]#keyctl# utility. The following is abbreviated example output from a Fedora system where UEFI Secure Boot is not enabled.

[subs="attributes"]
----
~]#{nbsp}keyctl list %:.builtin_trusted_keys
1 key in keyring:
265061799: ---lswrv   0   0 asymmetric: Fedora kernel signing key: ba8e2919f98f3f8e2e27541cde0d1f...
----

The following is abbreviated example output from a Fedora system where UEFI Secure Boot is enabled.

[subs="attributes"]
----
~]#{nbsp}keyctl list %:.builtin_trusted_keys
 5 keys in keyring:
 ...asymmetric: Microsoft Windows Production PCA 2011: a92902398e16c497...
 ...asymmetric: Fedora kernel signing key: ba8e2919f98f3f8e2e27541cde0d...
 ...asymmetric: Fedora Secure Boot CA: fde32599c2d61db1bf5807335d7b20e4...
 ...asymmetric: Red Hat Test Certifying CA: 08a0ef5800cb02fb587c12b4032...
 ...asymmetric: Microsoft Corporation UEFI CA 2011: 13adbf4309bd82709c8...
----

The above output shows the addition of two keys from the UEFI Secure Boot "`db`" keys plus the `Fedora Secure Boot CA` which is embedded in the `shim.efi` boot loader.

[[sect-kernel-module-authentication-requirements]]
==== Kernel Module Authentication Requirements

If UEFI Secure Boot is enabled or if the [option]`module.sig_enforce` kernel parameter has been specified, then only signed kernel modules that are authenticated using a key on the system key ring can be successfully loaded.footnote:[Provided that the public key is not on the system black list key ring.] If UEFI Secure Boot is disabled and if the [option]`module.sig_enforce` kernel parameter has not been specified, then unsigned kernel modules and signed kernel modules without a public key can be successfully loaded. This is summarized in xref:Working_with_Kernel_Modules.adoc#table-kernel-module-authentication-requirements-for-loading[Kernel Module Authentication Requirements for Loading].

[[table-kernel-module-authentication-requirements-for-loading]]
.Kernel Module Authentication Requirements for Loading

[options="header"]
|===
|Module Signed|Public Key Found and Signature Valid|UEFI Secure Boot State|module.sig_enforce|Module Load|Kernel Tainted
.3+|Unsigned
.3+|-
|Not enabled|Not enabled|Succeeds|Yes
|Not enabled|Enabled|Fails|
|Enabled|-|Fails|-
.3+|Signed
.3+|No
|Not enabled|Not enabled|Succeeds|Yes
|Not enabled|Enabled|Fails|-
|Enabled|-|Fails|-
.3+|Signed
.3+|Yes
|Not enabled|Not enabled|Succeeds|No
|Not enabled|Enabled|Succeeds|No
|Enabled|-|Succeeds|No
|===

Subsequent sections will describe how to generate a public and private X.509 key pair, how to use the private key to sign a kernel module, and how to enroll the public key into a source for the system key ring.

[[sect-generating-a-public-private-x509-key-pair]]
=== Generating a Public and Private X.509 Key Pair

You need to generate a public and private X.509 key pair that will be used to sign a kernel module after it has been built. The corresponding public key will be used to authenticate the kernel module when it is loaded.

. The [command]#openssl# tool can be used to generate a key pair that satisfies the requirements for kernel module signing in Fedora. Some of the parameters for this key generation request are best specified with a configuration file; follow the example below to create your own configuration file.
+
[subs="macros, attributes"]
----
~]#{nbsp}cat &lt;&lt; EOF &gt; configuration_file.config
[ req ]
default_bits = 4096
distinguished_name = req_distinguished_name
prompt = no
string_mask = utf8only
x509_extensions = myexts

[ req_distinguished_name ]
O = pass:quotes[_Organization_]
CN = pass:quotes[_Organization signing key_]
emailAddress = pass:quotes[_E-mail address_]

[ myexts ]
basicConstraints=critical,CA:FALSE
keyUsage=digitalSignature
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid
EOF
----

. After you have created the configuration file, you can create an X.509 public and private key pair. The public key will be written to the `pass:attributes[{blank}]_public_key_.der` file and the private key will be written to the `pass:attributes[{blank}]_private_key_.priv` file.
+
[subs="attributes"]
----
~]#{nbsp}openssl req -x509 -new -nodes -utf8 -sha256 -days 36500 \
 -batch -config configuration_file.config -outform DER \
 -out public_key.der \
 -keyout private_key.priv
----

. Enroll your public key on all systems where you want to authenticate and load your kernel module.

[WARNING]
====

Take proper care to guard the contents of your private key. In the wrong hands, the key could be used to compromise any system which has your public key.

====

[[sect-enrolling-public-key-on-target-system]]
=== Enrolling Public Key on Target System

When Fedora boots on a UEFI-based system with Secure Boot enabled, all keys that are in the Secure Boot db key database, but not in the dbx database of revoked keys, are loaded onto the system keyring by the kernel. The system keyring is used to authenticate kernel modules.

[[sect-factory-firmware-image-including-public-key]]
==== Factory Firmware Image Including Public Key

To facilitate authentication of your kernel module on your systems, consider requesting your system vendor to incorporate your public key into the UEFI Secure Boot key database in their factory firmware image.

[[sect-executable-key-enrollment-image-adding-public-key]]
==== Executable Key Enrollment Image Adding Public Key

It is possible to add a key to an existing populated and active Secure Boot key database. This can be done by writing and providing an EFI executable *enrollment* image. Such an enrollment image contains a properly formed request to append a key to the Secure Boot key database. This request must include data that is properly signed by the private key that corresponds to a public key that is already in the system's Secure Boot Key Exchange Key (KEK) database. Additionally, this EFI image must be signed by a private key that corresponds to a public key that is already in the key database.

It is also possible to write an enrollment image that runs under Fedora. However, the Fedora image must be properly signed by a private key that corresponds to a public key that is already in the KEK database.

The construction of either type of key enrollment images requires assistance from the platform vendor.

[[sect-system-administrator-manually-adding-public-key-to-the-mok-list]]
==== System Administrator Manually Adding Public Key to the MOK List

The Machine Owner Key (MOK) facility is a feature that is supported by Fedora and can be used to augment the UEFI Secure Boot key database. When Fedora boots on a UEFI-enabled system with Secure Boot enabled, the keys on the MOK list are also added to the system keyring in addition to the keys from the key database. The MOK list keys are also stored persistently and securely in the same fashion as the Secure Boot key database keys, but these are two separate facilities. The MOK facility is supported by shim.efi, MokManager.efi, grubx64.efi, and the Fedora [command]#mokutil# utility.

The major capability provided by the MOK facility is the ability to add public keys to the MOK list without needing to have the key chain back to another key that is already in the KEK database. However, enrolling a MOK key requires manual interaction by a *physically present* user at the UEFI system console on each target system. Nevertheless, the MOK facility provides an excellent method for testing newly generated key pairs and testing kernel modules signed with them.

Follow these steps to add your public key to the MOK list:

. Request addition of your public key to the MOK list using a Fedora userspace utility:
+
[subs="attributes"]
----
~]#{nbsp}mokutil --import my_signing_key_pub.der
----
+
You will be asked to enter and confirm a password for this MOK enrollment request.

. Reboot the machine.

. The pending MOK key enrollment request will be noticed by `shim.efi` and it will launch `MokManager.efi` to allow you to complete the enrollment from the UEFI console. You will need to enter the password you previously associated with this request and confirm the enrollment. Your public key is added to the MOK list, which is persistent.

Once a key is on the MOK list, it will be automatically propagated to the system key ring on this and subsequent boots when UEFI Secure Boot is enabled.

[[sect-signing-kernel-module-with-the-private-key]]
=== Signing Kernel Module with the Private Key

There are no extra steps required to prepare your kernel module for signing. You build your kernel module normally. Assuming an appropriate Makefile and corresponding sources, follow these steps to build your module and sign it:

. Build your `my_module.ko` module the standard way:
+
[subs="attributes"]
----
~]#{nbsp}make -C /usr/src/kernels/$(uname -r) M=$PWD modules
----

. Sign your kernel module with your private key. This is done with a C application. Note that the application requires that you provide both the files that contain your private and the public key as well as the kernel module file that you want to sign.
+
[subs="attributes"]
----
~]#{nbsp}/usr/src/kernels/$(uname -r)/scripts/sign-file \
 sha256 \
 my_signing_key.priv \
 my_signing_key_pub.der \
 my_module.ko
----

Your kernel module is in ELF image format and this application computes and appends the signature directly to the ELF image in your `my_module.ko` file. The [command]#modinfo# utility can be used to display information about the kernel module's signature, if it is present. For information on using the utility, see xref:Working_with_Kernel_Modules.adoc#sec-Displaying_Information_About_a_Module[Displaying Information About a Module].

Note that this appended signature is not contained in an ELF image section and is not a formal part of the ELF image. Therefore, tools such as [command]#readelf# will not be able to display the signature on your kernel module.

Your kernel module is now ready for loading. Note that your signed kernel module is also loadable on systems where UEFI Secure Boot is disabled or on a non-UEFI system. That means you do not need to provide both a signed and unsigned version of your kernel module.

[[sect-loading-signed-kernel-module]]
=== Loading Signed Kernel Module

Once your public key is enrolled and is in the system keyring, the normal kernel module loading mechanisms will work transparently. In the following example, you will use [command]#mokutil# to add your public key to the MOK list and you will manually load your kernel module with [command]#modprobe#.

. Optionally, you can verify that your kernel module will not load before you have enrolled your public key. First, verify what keys have been added to the system key ring on the current boot by running the [command]#keyctl list %:.builtin_trusted_keys# as root. Since your public key has not been enrolled yet, it should not be displayed in the output of the command.

. Request enrollment of your public key.
+
[subs="attributes"]
----
~]#{nbsp}mokutil --import my_signing_key_pub.der
----

. Reboot, and complete the enrollment at the UEFI console.
+
[subs="attributes"]
----
~]#{nbsp}reboot
----

. After the system reboots, verify the keys on the system key ring again.
+
[subs="attributes"]
----
~]#{nbsp}keyctl list %:.builtin_trusted_keys
----

. You should now be able to load your kernel module successfully.
+
[subs="macros, attributes"]
----
~]#{nbsp}modprobe -v my_module
insmod /lib/modules/3.17.4-302.fc21.x86_64/extra/pass:quotes[_my_module_].ko
~]#{nbsp}lsmod | grep my_module
pass:quotes[_my_module_] 12425 0
----

[[s1-kernel-modules-additional-resources]]
== Additional Resources

For more information on kernel modules and their utilities, see the following resources.

.Manual Page Documentation

* `lsmod(8)` — The manual page for the [command]#lsmod# command.

* `modinfo(8)` — The manual page for the [command]#modinfo# command.

* `modprobe(8)` — The manual page for the [command]#modprobe# command.

* `rmmod(8)` — The manual page for the [command]#rmmod# command.

* `ethtool(8)` — The manual page for the [command]#ethtool# command.

* `mii-tool(8)` — The manual page for the [command]#mii-tool# command.

.Installable and External Documentation

* link:++http://tldp.org/HOWTO/Module-HOWTO/++[Linux Loadable Kernel Module HOWTO] — The [citetitle]_Linux Loadable Kernel Module HOWTO_ from the Linux Documentation Project contains further information on working with kernel modules.
